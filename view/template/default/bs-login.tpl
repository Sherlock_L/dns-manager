<?php
use restphp\tpl\RestTpl;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>会员中心-软盛科技</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- css lib start -->
    <?php RestTpl::load('public-css-lib.tpl'); ?>
    <!-- css lib end -->
</head>
<body>
<!-- header section start -->
<div class="header_section">
    <?php RestTpl::load('public-head.tpl'); ?>
</div>
<!-- header section end -->

<!-- contact section start -->
<div class="contact_section layout_padding" id="message">
    <div class="container-fluid">
        <div class="contact_text"><b>会员登录</b></div>
        <div class="contact_section2">
            <div class="row">
                <div class="col-md-6 padding_left_0">
                    <div class="map-responsive">
                        <iframe src="" width="600" height="600" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mail_section">

                        <el-row>
                            <input type="text" class="mail_text" v-model="form.account" placeholder="请输入您的账号" name="Account">
                            <input type="password" class="mail_text" v-model="form.pwd" placeholder="请输入您的账号登录密码" name="Password">
                            <input type="text" class="rs-rand-text" v-model="form.randCode" placeholder="输入验证码" name="Rand Code">
                            <a href="javascript:void(0);" @click="refreshCode">
                                <img :src="randCodeUrl" class="rs-rand-img" />
                            </a>
                        </el-row>
                        <div class="send_bt"><a href="javascript:void(0);" title="00点击登录" @click="sendMessage">确认登录</a></div>
                        <div class="Locations_text" style="display: flex">
                            <div style="width: 50%;">
                                <!--<i class="el-icon-info"></i><a href="member-forget.html"><span class="map_icon">忘记密码</span></a>-->
                                <i class="el-icon-circle-plus"></i><a href="member-register.html"><span class="map_icon">注册账号</span></a>
                            </div>
                            <div style="display: none">
                                <i class="el-icon-circle-plus"></i><a href="member-register.html"><span class="map_icon">注册账号</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- contact section end -->
<!-- footer section start -->
<?php RestTpl::load('public-foot.tpl'); ?>
<!--  footer section end -->
<!-- js lib start -->
<?php RestTpl::load('public-js-lib.tpl'); ?>
<!-- js lib end-->

<script src="js/bs-member-login.js"></script>
</body>
</html>