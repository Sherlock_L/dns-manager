<style type="text/css">
    .dns-manager-statistics-member-title {
        width: 100px;
        background-color: #00b3ee;
        color: #ffffff;
        padding-top: 110px;
        text-align: center;
        font-size: 20px;
        font-weight: bolder;
        border-bottom-left-radius: 20px;
        border-top-left-radius: 20px
    }
    .dns-manager-statistics-member-trend-title {
        height: 60px;
        background-color: #00b3ee;
        line-height: 60px;
        font-weight: bolder;
        font-size: 20px;
        color: #ffffff;
        text-align: center;
        border-top-right-radius: 20px;
        border-top-left-radius: 20px
    }
    .dns-manager-statistics-domain-title {
        width: 100px;
        background-color: #eeab00;
        color: #ffffff;
        padding-top: 110px;
        text-align: center;
        font-size: 20px;
        font-weight: bolder;
        border-bottom-left-radius: 20px;
        border-top-left-radius: 20px
    }
    .dns-manager-statistics-record-title {
        width: 100px;
        background-color: #eeab00;
        color: #ffffff;
        padding-top: 110px;
        text-align: center;
        font-size: 20px;
        font-weight: bolder;
        border-bottom-right-radius: 20px;
        border-top-right-radius: 20px
    }
</style>
<div id="manage-index">
    <el-row>
        <el-col :span="2">
            &nbsp;
        </el-col>
        <!-- 会员统计 开始 -->
        <el-col :span="8">
            <el-container style="border-radius: 20px; background-color: #e7e7e7; height: 280px">
                <el-aside class="dns-manager-statistics-member-title" style="width: 100px">
                    会员<br />
                    (<span v-html="data.member.normal + data.member.refuse + data.member.uncheck + data.member.forbidden"></span>)
                </el-aside>
                <el-container>
                    <el-main>
                        <div id="member-draw" style="height: 160px"></div>
                    </el-main>
                    <el-footer>
                        <div style="line-height: 40px; text-align: center; font-size: 18px; font-weight: bold">
                            今日注册：<span style="color: goldenrod" v-html="data.member.today">0</span> 个
                        </div>
                    </el-footer>
                </el-container>
            </el-container>
        </el-col>
        <!-- 会员统计 结束 -->
        <el-col :span="2">
            &nbsp;
        </el-col>
        <!-- 会员注册趋势 开始 -->
        <el-col :span="8">
            <el-container style="border-radius: 20px; background-color: #e7e7e7; height: 280px">
                <el-header class="dns-manager-statistics-member-trend-title">会员注册趋势</el-header>
                <el-main>
                    <div id="member-trend" style="height: 180px; width: 100%"></div>
                </el-main>
            </el-container>
        </el-col>
        <!-- 会员注册趋势 结束 -->
    </el-row>

    <el-row style="margin-top: 30px">
        <el-col :span="2">
            &nbsp;
        </el-col>
        <!-- 域名统计 开始 -->
        <el-col :span="8">
            <el-container style="border-radius: 20px; background-color: #e7e7e7; height: 280px">
                <el-aside class="dns-manager-statistics-domain-title" style="width: 100px">
                    域名<br />
                    (<span v-html="data.domain.normal + data.domain.stop"></span>)
                </el-aside>
                <el-main>
                    <div id="domain-draw" style="height: 230px"></div>
                </el-main>
            </el-container>
        </el-col>
        <!-- 域名统计 结束 -->
        <el-col :span="2">
            &nbsp;
        </el-col>
        <!-- 解析统计 开始 -->
        <el-col :span="8">
            <el-container style="border-radius: 20px; background-color: #e7e7e7; height: 280px">
                <el-main>
                    <div id="record-draw" style="height: 240px"></div>
                </el-main>
                <el-aside class="dns-manager-statistics-record-title" style="width: 100px">
                    解析<br />
                    (<span v-html="data.record.total"></span>)
                </el-aside>
            </el-container>
        </el-col>
        <!-- 解析统计 结束 -->
    </el-row>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script src="js/bk-manage-index.js"></script>