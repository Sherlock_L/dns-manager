$(function(){

	var attr_data = $(".product-rg").attr("attr-data");
	if(attr_data == 'licence'){
		$txt_html = '<div class="product-title"><h4>电信许可证产品</h4>'+
						'<ul class="menu-ul">'+
							'<li class="licence current"><a href="../winlicence.html" class="icon3"><span>增值电信许可证申请方案</span></li>'+
							'<li class="idc "><a href="../winsafetyidc.html" class="icon3"><span>软盛IDC信息安全管理系统</span></li>'+
							'<li class="ircs "><a href="../winsafetyircs.html" class="icon3"><span>软盛云信息安全管理系统</span></li>'+
							'<li class="safecdn "><a href="../winsafetycdn.html" class="icon3"><span>软盛CDN信息安全管理系统</span></li>'+
							'<li class="icp "><a href="../winicp.html" class="icon3"><span>软盛ICP/IP备案管理系统</span></li>'+
							'<li class="internet "><a href="../wininternet.html" class="icon3"><span>软盛互联网基础资源支撑系统</span></li>'+
							// '<li class="safety "><a href="../winsafety.html" class="icon3"><span>软盛信息安全管理系统</span></li>'+
						'</ul>'+
					'</div>';
	}else if(attr_data == 'dengbao'){
		$txt_html = '<div class="product-title"><h4>公安等保产品</h4>'+
						'<ul class="menu-ul">'+
							'<li class="grade current"><a href="../wingradeprotect.html" class="icon3"><span>软盛等级保护建设解决方案</span></li>'+
							'<li class="securefirewall "><a href="../winsecurefirewall.html" class="icon3"><span>软盛网络安全防火墙(二代防火墙)</span></li>'+
							'<li class="safelogs "><a href="../winsafelogs.html" class="icon3"><span>软盛运维安全审计系统(堡垒机)</span></li>'+
							'<li class="logs "><a href="../winlogs.html" class="icon3"><span>软盛综合日志审计系统</span></li>'+
						'</ul>'+
					'</div>';
	}else if(attr_data == 'safety'){
		$txt_html = '<div class="product-title"><h4>信息安全产品</h4>'+
						'<ul class="menu-ul">'+
							'<li class="iis current"><a href="../winiis.html" class="icon3"><span>软盛非法信息拦截系统</span></li>'+
							'<li class="white "><a href="../winwhitelist.html" class="icon3"><span>软盛备案白名单系统</span></li>'+
							'<li class="waf "><a href="../winwaf.html" class="icon3"><span>软盛Web应用防火墙</span></li>'+
							'<li class="ig "><a href="../winig.html" class="icon3"><span>软盛防篡改系统</span></li>'+
						'</ul>'+
					'</div>';
	}else{
		$txt_html = ' <div class="product-title"><h4>IDC管理产品</h4></div>'+
				'<ul class="menu-ul">'+
				// '<li class="safety current"><a href="winsafety.html" class="icon3"><span>软盛信息安全管理系统</span></li>'+
				// '<li class="icp"><a href="winicp.html" ><span>软盛ICP/IP备案管理系统</span></a></li>'+
				// '<li class="internet"><a href="wininternet.html" class="icon3"><span>软盛互联网基础资源支撑系统</span></a></li>'+
				// '<li class="grade"><a href="wingradeprotect.html" class="icon3"><span>软盛等级保护建设解决方案</span></li>'+
				// '<li class="securefirewall"><a href="winsecurefirewall.html" class="icon3"><span>软盛网络安全防火墙(下一代防火墙)</span></a></li>'+
				// '<li class="logs"><a href="winlogs.html" class="icon3"><span>软盛综合日志审计系统</span></li>'+
    //     		'<li class="safelogs"><a href="winsafelogs.html" class="icon3"><span>软盛运维安全审计系统</span></li>'+
        		'<li class="ipv6"><a href="../winipv6.html" class="icon3"><span>软盛IPv6解决方案</span></li>'+
        		'<li class="cdn"><a href="wincdn/index.html" class="icon3"><span>软盛CDN管理系统</span></a></li>'+
        		'<li class="cms"><a href="../cloud.html" class="icon3"><span>软盛云主机管理系统</span></a></li>'+       		
        		'<li class="mydns"><a href="../winmydns.html" class="icon3"><span>软盛智能域名解析系统</span></a></li>'+
        		
    //     		'<li class="ig"><a href="winig.html" class="icon1"><span>软盛防篡改系统</span></a></li>'+
    //    			'<li class="waf"><a href="winwaf.html" class="icon1"><span>软盛Web应用防火墙</span></a></li>'+
				// '<li class="iis"><a href="winiis.html" class="icon3"><span>软盛非法信息拦截系统</span></a></li>'+
				
				
				// '<li class="isp"><a href="winisp.html" ><span>软盛接入资源管理系统</span></a></li>'+
				// '<li class="virtural"><a href="winvirtualhost.html" class="icon3"><span>软盛虚拟主机管理系统</span></a></li>'+
				
				'</ul>';
	}

	



	


	$(".nav-chanpin").append($txt_html);

});