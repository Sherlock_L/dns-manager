/**
 * 管理后台中心首页.
 * @type {Vue}
 */
let vmManageDomain = new Vue({
    el: '#manage-domains',
    data: function () {
        return {
            //列表所需数据
            formQuery: {
                domain: '',
                username: '',
                yn: ''
            },
            listLoading: false,
            tableData : [],
            page: {
                current: 1,
                total: 0,
                page:1,
                pageSize:20
            },
            selectRows: [],

            //新增|修改
            isNew: false,
            dialogFormVisible: false,
            form: {
                origin: '',
                yn: 1,
                topdate: '',
                enddate: '',
                username: ''
            },
            tmpForm: {
                origin: '',
                yn: 1,
                topdate: '',
                enddate: '',
                username: ''
            },
            rules: {
                origin: [
                    { required: true, message: '请输入域名', trigger: 'blur' }
                ],
                username: [
                    { required: true, message: '请输入用户账号', trigger: 'blur' }
                ],
                topdate: [
                    { required: true, message: '请选择开始时间', trigger: 'blur' }
                ],
                enddate: [
                    { required: true, message: '请选择结束时间', trigger: 'blur' }
                ],
                yn: [
                    { required: true, message: '请选择状态', trigger: 'blur' }
                ]
            },

            //修改状态
            dialogBatchStateFormVisible: false,
            stateForm: {
                yn: 1
            },
            stateRule: {
                yn: [
                    { required: true, message: '请选择状态', trigger: 'blur' }
                ]
            }

        }
    },
    methods: {
        /**
         * 架载统计数据.
         */
        loadData () {
            this.listLoading = true;
            let url = '/api/manage/domains?';
            let param = $.extend({}, this.formQuery, {page: this.page.page, size: this.page.pageSize});
            url += QiServletUtils.mapToQueryString(param);
            url +=  (iFrameToken ? iFrameToken : ('&iframe-token=' + iFrameToken))
            QiRestClient.get(url, (data) => {
                this.tableData = data.items;
                this.page.total = data.total;
                this.listLoading = false;
            }, (data) => {
                this.$message.error(data.message);
                this.listLoading = false;
            });
        },
        /**
         * 查询触发.
         */
        onQuery () {
            this.page.current = 1;
            this.page.page = 1;
            this.loadData();
        },
        /**
         * 分页触发事件.
         * @param val
         */
        handleCurrentChange (val) {
            this.page.page = val;
            console.log("当前页数："+val);
            this.loadData();
        },
        /**
         * 选择事件.
         */
        handleSelectionChange (val) {
            this.selectRows = val;
        },
        /**
         * 新增.
         */
        showAdd () {
            this.isNew = true;
            this.form = Object.assign({}, this.tmpForm);
            this.dialogFormVisible = true;
        },
        /**
         * 标注.
         * @param row
         */
        showEdit (row) {
            this.isNew = false;
            this.form = {
                id: row.id,
                origin: row.origin.substring(0, row.origin.length - 1),
                yn: parseInt(row.yn),
                username: row.username,
                topdate: QiDateUtils.strToStr(row.topdate, 'yyyy-MM-dd HH:mm:ss'),
                enddate: QiDateUtils.strToStr(row.enddate, 'yyyy-MM-dd HH:mm:ss')
            };

            this.dialogFormVisible = true;
        },
        /**
         * 保存数据操作.
         */
        saveEdit () {
            this.$refs['form'].validate((valid) => {
                if (valid) {
                    let success =  (data) => {
                        this.$message.success('操作成功');
                        this.dialogFormVisible = false;
                        this.loadData();
                    };
                    let error = (data) => {
                        this.$message.error(data.message);
                    };

                    if (this.isNew) {
                        QiRestClient.post('/api/manage/domains' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), this.form, success, error);
                    } else {
                        QiRestClient.put('/api/manage/domains/' + this.form.id + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), this.form, success, error);
                    }
                }
            });
        },
        /**
         * 详情.
         * @param row
         */
        goToSee (row) {
            let url = '?menu=2-2&domain=' + row.origin.substring(0, row.origin.length - 1);
            let iframeToken = QiServletUtils.get('iframe-token');
            if (!QiStringUtils.isBlank(iframeToken)) {
                url += "&iframe-token=" + iframeToken;
            }
            url +=  (iFrameToken ? iFrameToken : ('&iframe-token=' + iFrameToken))
            location.href = url;
        },
        /**
         * 显示状态修改.
         */
        showMark () {
            if (this.selectRows.length < 1) {
                this.$message.error('请选择需要删除的域名.');
                return;
            }
            this.dialogBatchStateFormVisible = true;
        },
        /**
         * 保存状态修改.
         */
        saveMark () {
            this.$refs['stateForm'].validate((valid) => {
                if (valid) {
                    let success =  (data) => {
                        this.$message.success('操作成功');
                        this.dialogBatchStateFormVisible = false;
                        this.loadData();
                    };
                    let error = (data) => {
                        this.$message.error(data.message);
                    };

                    let ids = [];
                    this.selectRows.forEach((row) => {
                        ids.push(row.id);
                    });

                    let data = {
                        yn: this.stateForm.yn,
                        domainIdList: ids
                    }

                    QiRestClient.post('/api/manage/domains/actions/state' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), data, success, error);
                }
            });
        },
        /**
         * 删除选中域名.
         */
        deleteAll () {
            if (this.selectRows.length < 1) {
                this.$message.error('请选择需要删除的域名.');
                return;
            }
            this.$confirm('确定需要移除选中域名吗？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then( () => {
                let domains = [];
                this.selectRows.forEach((row) => {
                    domains.push(row.id);
                });
                QiRestClient.post('/api/manage/domains/actions/delete' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)),  domains, () => {
                    this.$message.success('操作成功');
                    this.loadData();
                },  (data) => {
                    this.$message.error(data.message);
                })
            });
        },
        /**
         * 删除.
         * @param row
         */
        deleteMessage (row) {
            let self = this;
            this.$confirm('确定需要移除选中记录吗？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then( () => {
                QiRestClient.delete('/api/manage/domains/' + row.id + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)),  () => {
                    self.$message.success('操作成功');
                    self.loadData();
                },  (data) => {
                    self.$message.error(data.message);
                })
            });
        }
    },
    mounted: function () {
        let username = QiServletUtils.get("username");
        if (!QiStringUtils.isBlank(username)) {
            this.formQuery.username = username;
        }
        this.loadData();
    }
});