/**
 * 会员中心首页.
 * @type {Vue}
 */
let vmMemberInfo = new Vue({
    el: '#member-info',
    data: function () {
        let validateMobile =  (rule, value, callback) => {
            if (QiStringUtils.isMobile(value)) {
                callback();
            } else {
                callback(new Error(rule.message));
            }
        };

        return {
            form: {
                password: ''
            },
            rules: {
                realname: [
                    { required: true, message: '请输入真实姓名', trigger: 'blur' },
                    { min: 1, max: 20, message: '真实姓名长度为1-20个字符', trigger: 'blur' }
                ],
                tel: [
                    { required: true, message: '请输入手机号码', trigger: 'blur' },
                    { validator: validateMobile, message: '请输入正确的手机号码', trigger: 'blur' }
                ]
            }
        }
    },
    methods: {
        /**
         * 架载统计数据.
         */
        loadData () {
            QiRestClient.get('/api/users/_self', (data) => {
                this.form = Object.assign(data, {password: ''});
            })
        },

        /**
         * 保存修改.
         */
        saveModify () {
            this.$refs['form'].validate((valid) => {
                if (valid) {
                    let success =  (data) => {
                        this.$message.success('操作成功');
                        this.dialogFormVisible = false;
                        this.loadData();
                    };
                    let error = (data) => {
                        this.$message.error(data.message);
                    };
                    let data = {
                        realname: this.form.realname,
                        tel: this.form.tel
                    };
                    if (!QiStringUtils.isBlank(this.form.password)) {
                        data.password = this.form.password;
                    }
                    QiRestClient.put('/api/users/_self', data, success, error);
                }
            });
        }
    },
    mounted: function () {
        this.loadData();
    }
});