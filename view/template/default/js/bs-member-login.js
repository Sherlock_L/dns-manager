new Vue({
    el: '#message',
    data: function () {
        return {
            form : {
                account: '',
                pwd: '',
                randCode: ''
            },
            randCodeUrl: '/members/randcode'
        }
    },
    methods: {
        /**
         * 发送留言.
         */
        sendMessage () {
            //参数校验.
            if (QiStringUtils.isBlank(this.form.account)) {
                this.$message.error('请填写您的账号');
                return;
            }
            if (QiStringUtils.isBlank(this.form.pwd)) {
                this.$message.error('请填写账号密码');
                return;
            }
            if (QiStringUtils.isBlank(this.form.randCode)) {
                this.$message.error('请填写验证码');
                return;
            }

            let url = '/api/user/session';
            QiRestClient.post(url, this.form,  (data) => {
                this.$message.success('登录成功，正为您跳转...');
                this.form = {
                    name: '',
                    product: '',
                    mobile: '',
                    more: ''
                };
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }, (data) => {
                this.$message.error(data.message);
                this.refreshCode();
            });
        },

        /**
         * 刷新验证码.
         */
        refreshCode () {
            this.randCodeUrl = '/members/randcode?_p=' + (new Date()).getTime();
        }
    }
});