/**
 * 管理后台中心首页.
 * @type {Vue}
 */
let vmManageMember = new Vue({
    el: '#manage-member',
    data: function () {
        let validateMobile =  (rule, value, callback) => {
            if (QiStringUtils.isMobile(value)) {
                callback();
            } else {
                callback(new Error(rule.message));
            }
        };

        return {
            //列表所需数据
            formQuery: {
                account: '',
                mobile: '',
                realName: '',
                remark: ''
            },
            listLoading: false,
            tableData : [],
            page: {
                current: 1,
                total: 0,
                page:1,
                pageSize:20
            },
            selectRows: [],
            //修改
            isNew: false,
            dialogFormVisible: false,
            form: {
                id: 0,
                isadmin: 'N',
                username: '',
                password: '',
                realname: '',
                tel: '',
                verify: 'Y',
                soanum: 1,
                rrnum: 20
            },
            tmpForm: {
                id: 0,
                isadmin: 'N',
                username: '',
                password: '',
                realname: '',
                tel: '',
                verify: 'Y',
                soanum: 1,
                rrnum: 20
            },
            rulesNew: {
                username: [
                    { required: true, message: '请输入会员账号', trigger: 'blur' },
                    { min: 4, max: 20, message: '会员账号长度为4-20个字符', trigger: 'blur' }
                ],
                password: [
                    { required: true, message: '请输入账号密码', trigger: 'blur' },
                    { min: 6, max: 16, message: '会员账号长度为6-16个字符', trigger: 'blur' }
                ],
                isadmin: [
                    { required: true, message: '请选择账号类型', trigger: 'blur' }
                ],
                realname: [
                    { required: true, message: '请输入真实姓名', trigger: 'blur' },
                    { min: 1, max: 20, message: '真实姓名长度为1-20个字符', trigger: 'blur' }
                ],
                tel: [
                    { required: true, message: '请输入手机号码', trigger: 'blur' }
                ],
                verify: [
                    { required: true, message: '请选择状态', trigger: 'blur' }
                ],
                soanum: [
                    { required: true, message: '请输入最大域名数', trigger: 'blur' }
                ],
                rrnum: [
                    { required: true, message: '请输入最大记录数', trigger: 'blur' }
                ]
            },
            rulesModify: {
                username: [
                    { required: true, message: '请输入会员账号', trigger: 'blur' },
                    { min: 4, max: 20, message: '会员账号长度为4-20个字符', trigger: 'blur' }
                ],
                isadmin: [
                    { required: true, message: '请选择账号权限', trigger: 'blur' }
                ],
                realname: [
                    { required: true, message: '请输入真实姓名', trigger: 'blur' },
                    { min: 1, max: 20, message: '真实姓名长度为1-20个字符', trigger: 'blur' }
                ],
                tel: [
                    { required: true, message: '请输入手机号码', trigger: 'blur' }
                ],
                verify: [
                    { required: true, message: '请选择状态', trigger: 'blur' }
                ],
                soanum: [
                    { required: true, message: '请输入最大域名数', trigger: 'blur' }
                ],
                rrnum: [
                    { required: true, message: '请输入最大记录数', trigger: 'blur' }
                ]
            },
            dialogBatchStateFormVisible: false,
            stateForm: {
                verify: 'Y'
            },
            stateRule: {
                verify: [
                    { required: true, message: '请选择状态', trigger: 'blur' }
                ]
            }
        }
    },
    methods: {
        /**
         * 架载统计数据.
         */
        loadData () {
            this.listLoading = true;
            let url = '/api/manage/users?';
            let param = $.extend({}, this.formQuery, {page: this.page.page, size: this.page.pageSize});
            url += QiServletUtils.mapToQueryString(param);
            url += (iFrameToken ? iFrameToken : ('&iframe-token=' + iFrameToken))
            QiRestClient.get(url, (data) => {
                this.tableData = data.items;
                this.page.total = data.total;
                this.listLoading = false;
            }, (data) => {
                this.$message.error(data.message);
                this.listLoading = false;
            });
        },
        /**
         * 查询触发.
         */
        onQuery () {
            this.page.current = 1;
            this.page.page = 1;
            this.loadData();
        },
        /**
         * 分页触发事件.
         * @param val
         */
        handleCurrentChange (val) {
            this.page.page = val;
            console.log("当前页数："+val);
            this.loadData();
        },
        /**
         * 选择事件.
         */
        handleSelectionChange (val) {
            this.selectRows = val;
        },
        /**
         * 标注.
         * @param row
         */
        edit (row) {
            this.isNew = false;
            this.form = Object.assign({}, row);
            this.form.password = '';
            this.dialogFormVisible = true;
        },
        /**
         * 新增.
         */
        showAdd () {
            this.isNew = true;
            this.form = Object.assign({}, this.tmpForm);
            this.dialogFormVisible = true;
        },
        /**
         * 保存数据操作.
         */
        saveEdit () {
            this.$refs['form'].validate((valid) => {
                if (valid) {
                    let success =  (data) => {
                        this.$message.success('操作成功');
                        this.dialogFormVisible = false;
                        this.loadData();
                    };
                    let error = (data) => {
                        this.$message.error(data.message);
                    };

                    if (this.isNew) {
                        QiRestClient.post('/api/manage/users' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), this.form, success, error);
                    } else {
                        QiRestClient.put('/api/manage/users/' + this.form.ID + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), this.form, success, error);
                    }
                }
            });
        },
        /**
         * 删除.
         */
        deleteAll () {
            if (this.selectRows.length < 1) {
                this.$message.error('请选择需要删除的用户.');
                return;
            }
            this.$confirm('确定需要移除选中用户吗？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then( () => {
                let users = [];
                this.selectRows.forEach((row) => {
                    users.push(row.ID);
                });
                QiRestClient.post('/api/manage/users/actions/delete' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)),  users, () => {
                    this.$message.success('操作成功');
                    this.loadData();
                },  (data) => {
                    this.$message.error(data.message);
                })
            });
        },
        /**
         * 显示审核.
         */
        showMark () {
            if (this.selectRows.length < 1) {
                this.$message.error('请选择需要审核的用户.');
                return;
            }
            this.dialogBatchStateFormVisible = true;
        },
        /**
         * 保存审核.
         */
        saveMark () {
            this.$refs['stateForm'].validate((valid) => {
                if (valid) {
                    let success =  (data) => {
                        this.$message.success('操作成功');
                        this.dialogBatchStateFormVisible = false;
                        this.loadData();
                    };
                    let error = (data) => {
                        this.$message.error(data.message);
                    };

                    let users = [];
                    this.selectRows.forEach((row) => {
                        users.push(row.ID);
                    });

                    let data = {
                        verify: this.stateForm.verify,
                        userIdList: users
                    }

                    QiRestClient.post('/api/manage/users/actions/state' + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)), data, success, error);
                }
            });
        },
        /**
         * 删除.
         * @param row
         */
        deleteMessage (row) {
            var self = this;
            this.$confirm('确定需要移除选中记录吗？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then( () => {
                QiRestClient.delete('/api/manage/users/' + row.ID + (iFrameToken ? iFrameToken : ('?iframe-token=' + iFrameToken)),  () => {
                    self.$message.success('操作成功');
                    self.loadData();
                },  (data) => {
                    self.$message.error(data.message);
                })
            });
        }
    },
    mounted: function () {
        this.loadData();
    }
});