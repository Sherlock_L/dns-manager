<div id="member-records-dns">
    <div style="padding: 20px;">

        <div style="border: 1px solid #f7f7f7;">
            <el-row>
                <el-form :inline="true" :model="formQuery" class="demo-form-inline" v-show="showFormQuery">
                    <el-form-item label="域名：">
                        <el-input v-model="formQuery.domain" size="small" style="width: 160px" placeholder="域名" clearable></el-input>
                    </el-form-item>
                    <el-form-item>
                        <el-button type="primary" size="small" @click="onQuery">查询</el-button>
                    </el-form-item>
                </el-form>
            </el-row>
            <el-row>
                <el-button-group>
                    <el-button type="danger" size="small" icon="el-icon-s-check" style="float: left" @click="deleteAll">批量删除</el-button>
                </el-button-group>
                <el-button type="primary" icon="el-icon-circle-plus-outline" style="float: right" size="small" @click="showAdd">新增</el-button>
            </el-row>
            <el-row style="margin-top: 10px">
                <template>
                    <el-table
                            :data="tableData"
                            style="width: 100%"
                            border
                            stripe
                            v-loading="listLoading"
                            size="small"
                            @selection-change="handleSelectionChange">
                        <el-table-column
                                type="selection"
                                width="55">
                        </el-table-column>
                        <el-table-column
                                prop="id"
                                width="80"
                                label="记录ID">
                        </el-table-column>
                        <el-table-column
                                prop="name"
                                label="主机名">
                        </el-table-column>
                        <el-table-column
                                prop="type"
                                width="80"
                                label="类型">
                        </el-table-column>
                        <el-table-column
                                prop="data"
                                label="值">
                        </el-table-column>
                        <el-table-column
                                prop="aux"
                                width="60"
                                label="优先级">
                        </el-table-column>
                        <el-table-column
                                prop="ttl"
                                width="60"
                                label="TTL">
                        </el-table-column>
                        <el-table-column
                                prop="netname"
                                width="80"
                                label="解析组">
                            <template scope="scope">
                                <span v-if="!scope.row.netname || scope.row.netname==''">默认</span>
                                <span v-else v-html="scope.row.netname"></span>
                            </template>
                        </el-table-column>
                        <el-table-column
                                prop="origin"
                                label="域名">
                        </el-table-column>
                        <el-table-column
                                width="300"
                                label="操作">
                            <template scope="scope">
                                <el-button type="primary" size="small" icon="el-icon-edit" @click="showEdit(scope.row)" plain>修改</el-button>
                                <el-button type="danger" size="small" icon="el-icon-delete" @click="deleteMessage(scope.row)" plain>删除</el-button>
                                <el-button type="success" size="small" icon="el-icon-share" @click="goToSee(scope.row)" plain>负载均衡</el-button>
                            </template>
                        </el-table-column>
                    </el-table>
                </template>
                <div class="page-block" >
                    <el-pagination
                            @current-change="handleCurrentChange"
                            layout="total,prev, pager, next"
                            :total= page.total
                            :page-size = page.pageSize
                            class="page"
                    >
                    </el-pagination>
                </div>
            </el-row>
        </div>

        <el-dialog :title="isNew ? '新增解析记录' : '修改记录'" :visible.sync="dialogFormVisible" width="650px">
            <el-form :model="form" ref="form" :rules="rules" label-width="160px" size="small">
                <el-form-item label="域名：" prop="origin" v-show="showFormQuery">
                    <el-input :disabled="!isNew" placeholder="请输入域名" v-model="form.origin" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="主机名：" prop="name">
                    <el-input placeholder="请输入主机名" v-model="form.name" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="类型：" prop="type">
                    <el-select v-model="form.type" placeholder="请选择类型" size="small">
                        <el-option v-for="item in typeList" :label="item" :value="item"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="优先级：" prop="aux">
                    <el-input-number placeholder="请输入优先级" v-model="form.aux" :min="0" size="small" :precision="0" style="width: 280px"></el-input-number>  如：80
                </el-form-item>
                <el-form-item label="TTL：" prop="ttl">
                    <el-input-number placeholder="请输入优先级" v-model="form.ttl" :min="0" size="small" :precision="0" style="width: 280px"></el-input-number>  如：80
                </el-form-item>
                <el-form-item label="解析组：" prop="netid">
                    <el-select v-model="form.netid" placeholder="请选择状态" size="small">
                        <el-option v-for="item in netList" :label="item.netname" :value="item.id"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="解析内容：" prop="data">
                    <el-input type="textarea" placeholder="请输入解析内容" v-model="form.data" size="small" style="width: 280px"></el-input>
                </el-form-item>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogFormVisible = false" size="small">取 消</el-button>
                <el-button type="primary" @click="saveEdit" size="small">确 定</el-button>
            </div>
        </el-dialog>

    </div>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script language="JavaScript" src="js/bm-member-records-dns.js"></script>