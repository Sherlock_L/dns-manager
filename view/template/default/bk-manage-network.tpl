<div id="manage-network">
    <div style="padding: 20px; margin-top: -50px">
        <div style="padding: 10px">
            <el-breadcrumb separator-class="el-icon-arrow-right">
                <el-breadcrumb-item>网络组管理（解析线路）</el-breadcrumb-item>
            </el-breadcrumb>
        </div>
        <div style="border: 1px solid #f7f7f7; padding: 30px">
            <el-row>
                <el-form :inline="true" :model="formQuery" class="demo-form-inline">
                    <el-form-item label="关键字：">
                        <el-input v-model="formQuery.keyword" size="small" style="width: 160px" placeholder="域名" clearable></el-input>
                    </el-form-item>
                    <el-form-item>
                        <el-button type="primary" size="small" @click="onQuery">查询</el-button>
                    </el-form-item>
                </el-form>
            </el-row>
            <el-row>
                <el-button-group>
                    <el-button type="danger" size="small" icon="el-icon-s-check" style="float: left" @click="deleteAll">批量删除</el-button>
                </el-button-group>
                <el-button type="primary" icon="el-icon-circle-plus-outline" style="float: right" size="small" @click="showAdd">新增</el-button>
            </el-row>
            <el-row style="margin-top: 10px">
                <template>
                    <el-table
                            :data="tableData"
                            style="width: 100%"
                            border
                            stripe
                            v-loading="listLoading"
                            size="small"
                            @selection-change="handleSelectionChange">
                        <el-table-column
                                type="selection"
                                width="55">
                        </el-table-column>
                        <el-table-column
                                prop="id"
                                width="80"
                                label="网络ID">
                        </el-table-column>
                        <el-table-column
                                prop="netname"
                                label="名称">
                        </el-table-column>
                        <el-table-column
                                prop="remark"
                                label="描述">
                        </el-table-column>
                        <el-table-column
                                prop="aux"
                                label="优先级">
                        </el-table-column>
                        <el-table-column
                                width="450"
                                label="操作">
                            <template scope="scope">
                                <el-button type="primary" size="small" icon="el-icon-edit" @click="showEdit(scope.row)" plain>修改</el-button>
                                <el-button type="danger" size="small" icon="el-icon-delete" @click="deleteMessage(scope.row)" plain>删除</el-button>
                                <el-button type="primary" size="small" icon="el-icon-s-operation" @click="goToSee(scope.row, 1)" plain>IPV4分配表</el-button>
                                <el-button type="primary" size="small" icon="el-icon-magic-stick" @click="goToSee(scope.row, 1)" plain>IPV6分配表</el-button>
                            </template>
                        </el-table-column>
                    </el-table>
                </template>
                <div class="page-block" >
                    <el-pagination
                            @current-change="handleCurrentChange"
                            layout="total,prev, pager, next"
                            :total= page.total
                            :page-size = page.pageSize
                            class="page"
                    >
                    </el-pagination>
                </div>
            </el-row>
        </div>

        <el-dialog :title="isNew ? '新增线路' : '修改线路'" :visible.sync="dialogFormVisible">
            <el-form :model="form" ref="form" :rules="rules" label-width="260px" size="small">
                <el-form-item label="名称：" prop="netname">
                    <el-input placeholder="请输入名称" v-model="form.netname" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="描述：" prop="remark">
                    <el-input placeholder="请输入描述" v-model="form.remark" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="优先级：" prop="aux">
                    <el-input-number placeholder="请输入优先级" :min="0" v-model="form.aux" :precision="0" size="small" style="width: 280px"></el-input-number>
                </el-form-item>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogFormVisible = false" size="small">取 消</el-button>
                <el-button type="primary" @click="saveEdit" size="small">确 定</el-button>
            </div>
        </el-dialog>

    </div>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script language="JavaScript" src="js/bk-manage-network.js"></script>