<?php
return array(
    'dev' => array(
        'TPL_DIR' => 'view/template/default',
        'TPL_CACHE_DIR' => 'runtime/template',
        'RECORD_LOG' => 1,
        'DATABASE_SQL_SERVER' => array(
            'DB_PREFIX' => '', //表前缀
            'Def_Select'	=> array(	//查询库，一般是从库或只读库
                'dbhost'	=> '192.168.1.70,1433',
                'dbuser'	=> 'winmydns',
                'dbpass'	=> '123456',
                'dbname'	=> 'WinMyDNS',
                'charset'	=> 'gb2312'
            ),
            'Def_Update'	=> array(	//有数据修改权限的库，一般指主库
                'dbhost'	=> '192.168.1.70,1433',
                'dbuser'	=> 'winmydns',
                'dbpass'	=> '123456',
                'dbname'	=> 'WinMyDNS',
                'charset'	=> 'gb2312'
            )
        )
    ),
    'pro' => array(
        'TPL_DIR' => 'view/template/default',
        'TPL_CACHE_DIR' => 'runtime/template',
        'DATABASE_SQL_SERVER' => array(
            'DB_PREFIX' => '', //表前缀
            'Def_Select'	=> array(	//查询库，一般是从库或只读库
                'dbhost'	=> '192.168.1.70,1433',
                'dbuser'	=> 'winmydns',
                'dbpass'	=> '123456',
                'dbname'	=> 'WinMyDNS',
                'charset'	=> 'gb2312'
            ),
            'Def_Update'	=> array(	//有数据修改权限的库，一般指主库
                'dbhost'	=> '192.168.1.70,1433',
                'dbuser'	=> 'winmydns',
                'dbpass'	=> '123456',
                'dbname'	=> 'WinMyDNS',
                'charset'	=> 'gb2312'
            )
        )
    )
);