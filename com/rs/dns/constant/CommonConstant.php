<?php
namespace com\rs\dns\constant;

class CommonConstant {
    const PARAM_ERROR = 'PARAM_ERROR';
    const DATA_TO_DELETE_CAN_NOT_NULL = 'DATA_TO_DELETE_CAN_NOT_NULL';
    const NO_RIGHT_TO_OPERATING_THE_DATA = 'NO_RIGHT_TO_OPERATING_THE_DATA';
}