<?php
namespace com\rs\dns\constant;

/**
 * Class TokenConstant
 * @package com\rs\dns\constant
 */
final class TokenConstant {
    const DEF_EXPIRE_TIME = 20 * 60;
}