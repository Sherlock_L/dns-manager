<?php
namespace com\rs\dns\constant;

/**
 * Class RecordConstant
 * @package com\rs\dns\constant
 */
final class RecordConstant {
    const RECORD_NOT_FOUND = 'RECORD_NOT_FOUND';
    const RECORD_DATA_NOT_NULL = 'RECORD_DATA_NOT_NULL';
    const RECORD_DATA_MUST_IPV4 = 'RECORD_DATA_MUST_IPV4';
    const RECORD_DATA_MUST_IPV6 = 'RECORD_DATA_MUST_IPV6';
    const RECORD_DATA_MUST_DOMAIN = 'RECORD_DATA_MUST_DOMAIN';
    const RECORD_DATA_MUST_SRV = 'RECORD_DATA_MUST_SRV';
    const RECORD_SAME_EXISTS = 'RECORD_SAME_EXISTS';
    const RECORD_SAME_URL_EXISTS = 'RECORD_SAME_URL_EXISTS';

    const RECORD_BALANCE_AUX_NOT_NULL = 'RECORD_BALANCE_AUX_NOT_NULL';
}