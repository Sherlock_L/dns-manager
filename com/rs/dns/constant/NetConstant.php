<?php
namespace com\rs\dns\constant;

/**
 * Class NetConstant
 * @package com\rs\dns\constant
 */
final class NetConstant {
    const NET_NAME_EXIST = 'NET_NAME_EXIST';
    const NET_NOT_EXISTS = 'NET_NOT_EXISTS';
}