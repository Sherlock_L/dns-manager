<?php
namespace com\rs\dns\constant;

class UserConstant {
    const CAN_NOT_ACCESS_OTHERS = '[CAN_NOT_ACCESS_OTHERS]';
    const CAN_NOT_ACCESS_OTHERS_CODE = 'BusinessAssistant/CAN_NOT_ACCESS_OTHERS';
    const OLD_PASSWORD_ERROR = '[OLD_PASSWORD_ERROR]';
    const OLD_PASSWORD_ERROR_CODE = 'BusinessAssistant/OLD_PASSWORD_ERROR';
    const DEF_IS_ADMIN = 'N';
    const DEF_SOA_NUM = 1;
    const DEF_RR_NUM = 20;
}