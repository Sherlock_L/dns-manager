<?php
namespace com\rs\dns\constant;

/**
 * Class HostConstant
 * @package com\rs\dns\constant
 */
final class HostConstant {
    const HOST_IP_NOT_SAME = 'HOST_IP_NOT_SAME';
}