<?php
namespace com\rs\dns\service;

use com\rs\dns\repository\ConfigRepository;

/**
 * Class ConfigService
 * @package com\rs\dns\service
 */
final class ConfigService {
    /**
     * 获取所有配置列表.
     * @return array
     * @throws \restphp\exception\RestException
     */
    public static function getAll() {
        $configRepository = new ConfigRepository();
        return $configRepository->findAll(array());
    }

    /**
     * 批量设置配置.
     * @param array $listConfig format: [{configCode:'xxx',configValue:'xxx'}, ...]
     * @throws \restphp\exception\RestException
     */
    public static function batchSet($listConfig) {
        $configRepository = new ConfigRepository();
        foreach ($listConfig as $config) {
            $existsRule = array('configCode' => $config['configCode']);
            $exists = $configRepository->findOne($existsRule);
            $arrData = array('configValue' => $config['configValue']);
            if (null!=$exists && !empty($exists)) {
                $arrUpdateRule = array('configCode' => $config['configCode']);
                $configRepository->update($arrData, $arrUpdateRule);
            } else {
                $arrData['configCode'] = $config['configCode'];
                $configRepository->insert($arrData);
            }
        }
    }

    /**
     * 获取配置值.
     * @param $configCode
     * @return mixed|null
     * @throws \restphp\exception\RestException
     */
    public static function getConfigValue($configCode) {
        $configRepository = new ConfigRepository();
        $config = $configRepository->findOne(array(
            'configCode' => $configCode
        ));
        return null == $config ? null : $config['configValue'];
    }
}