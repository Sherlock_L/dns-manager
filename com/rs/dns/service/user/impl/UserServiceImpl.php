<?php
namespace com\rs\dns\service\user\impl;

use com\rs\dns\constant\UserConstant;
use com\rs\dns\constant\UserManageConst;
use com\rs\dns\repository\bean\UserListBean;
use com\rs\dns\repository\UserListRepository;
use com\rs\dns\service\user\UserService;
use com\rs\dns\utils\EncryptUtils;
use restphp\exception\RestException;
use restphp\http\RestHttpStatus;

class UserServiceImpl implements UserService {
    /**
     * modify user info.
     * @param $userId
     * @param $arrUpdate
     * @return mixed|void
     * @throws RestException
     */
    public function modify($userId, $arrUpdate) {
        $userRepository = new UserListRepository();
        $userBean = $userRepository->findOneByUserId($userId);
        if (null == $userBean) {
            throw new RestException(UserManageConst::USER_NOT_EXIST, UserManageConst::USER_NOT_EXIST_CODE, RestHttpStatus::Not_Found);
        }

        if (isset($arrUpdate['password'])) {
            $arrUpdate['password'] = EncryptUtils::encodePassword($arrUpdate['password']);
        }

        if (isset($arrUpdate['tel'])) {
            $arrExistPhone = array(
                'rule' => array(
                    'userId' => array(
                        'id<>?',
                        $userId
                    ),
                    'tel' => $arrUpdate['tel']
                )
            );
            $nExistPhone = $userRepository->count($arrExistPhone);
            if ($nExistPhone > 0) {
                throw new RestException(UserManageConst::PHONE_EXIST, UserManageConst::PHONE_EXIST_CODE);
            }
        }

        $userRepository->update($arrUpdate, array('id' => $userId));
    }

    /**
     * 获取用户信息.
     * @param int $userId
     * @return UserListBean|null
     */
    public function getInfo($userId) {
        $userRepository = new UserListRepository();
        return $userRepository->findOneByUserId($userId);
    }

    /**
     * 通过用户命获取用户信息.
     * @param string $username
     * @return UserListBean|mixed|null
     */
    public function getInfoByUsername($username) {
        $userRepository = new UserListRepository();
        return $userRepository->findOneByUserAccount($username);
    }


    /**
     * modify password.
     * @param $userId
     * @param $oldPassword
     * @param $newPassword
     * @return mixed|void
     * @throws RestException
     */
    public function modifyPassword($userId, $oldPassword, $newPassword) {
        $userRepository = new UserListRepository();
        $userBean = $userRepository->findOneByUserId($userId);
        if (null == $userBean) {
            throw new RestException(UserManageConst::USER_NOT_EXIST, UserManageConst::USER_NOT_EXIST_CODE, RestHttpStatus::Not_Found);
        }

        if (strtolower($userBean->getPasswd()) != EncryptUtils::encodePassword($oldPassword)) {
            throw new RestException(UserConstant::OLD_PASSWORD_ERROR, UserConstant::OLD_PASSWORD_ERROR_CODE);
        }

        $arrUpdate = array(
            'passwd' => EncryptUtils::encodePassword($newPassword)
        );

        $this->modify($userBean, $arrUpdate);
    }

    /**
     * 接口创建用户.
     * @param string $username
     * @param string $pwd
     * @return mixed|void
     */
    public function apiCreate($username, $pwd) {
        $arrUser = array(
            'username' => $username,
            'password' => $pwd,
            'realname' => $username,
            'isadmin' => UserConstant::DEF_IS_ADMIN,
            'soanum' => UserConstant::DEF_SOA_NUM,
            'rrnum' => UserConstant::DEF_RR_NUM,
            'verify' => 'Y',
            'regtime' => date('Y-m-d H:i:s')
        );

        $userRepository = new UserListRepository();
        $userRepository->insert($arrUser);
    }


}