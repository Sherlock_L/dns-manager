<?php
/**
 * Created by zj.
 * User: zj
 * Date: 2019/11/13 0013
 * Time: 上午 10:33
 */
namespace com\rs\dns\service\user\impl;

use com\rs\dns\constant\UserLogTypeConstant;
use com\rs\dns\constant\UserSessionErrorConstant;
use com\rs\dns\controller\api\vo\UserLoginForm;
use com\rs\dns\exception\BaException;
use com\rs\dns\repository\bean\UserListBean;
use com\rs\dns\repository\UserLogRepository;
use com\rs\dns\repository\UserListRepository;
use com\rs\dns\service\MemberRandCodeService;
use com\rs\dns\service\user\UserSessionService;
use com\rs\dns\utils\EncryptUtils;
use restphp\exception\RestException;
use restphp\http\request\RestHttpRequestIP;
use restphp\http\RestHttpStatus;
use restphp\i18n\RestLangUtils;
use restphp\utils\RestUUIDUtil;

/**
 * Class UserSessionServiceImpl
 * @package php\service\user\impl
 */
final class UserSessionServiceImpl implements UserSessionService {
    private $_userRepository;
    private $_sessionKey = "_U_S_";

    function __construct() {
        $this->_userRepository = new UserListRepository();
    }

    /**
     * 是否已登录.
     * @return bool
     */
    public function isLogin() {
        if (!isset($_SESSION[$this->_sessionKey])) {
            return false;
        }
        $object = $_SESSION[$this->_sessionKey];
        return !is_null($object);
    }

    /**
     * 是否为管理员.
     * @return bool.
     */
    public function isAdmin() {
        $userBean = $this->getSession();
        if (null == $userBean) {
            return false;
        }
        return $userBean->getIsadmin() == "Y";
    }

    /**
     * 是否为超管.
     * @return bool.
     */
    public function isSuper() {
        $userBean = $this->getSession();
        if (null == $userBean) {
            return false;
        }
        return $userBean->getIsadmin() == "Y";
    }


    /**
     * 获取Session.
     * @return UserListBean|null
     */
    public function getSession() {
        if (!isset($_SESSION[$this->_sessionKey])) {
            return null;
        }
        $object = $_SESSION[$this->_sessionKey];
        if (is_null($object)) {
            return null;
        }
        return unserialize($object);
    }

    /**
     * 用户登录.
     * @param UserLoginForm $oUserSession
     * @throws RestException.
     */
    public function login($oUserSession) {
        // 验证码
        if (!MemberRandCodeService::checkCode($oUserSession->getRandCode())) {
            throw new BaException(UserSessionErrorConstant::USER_LOGIN_RAND_CODE_ERROR);
        }

        // 获取用户
        $userBean = $this->_userRepository->findOneByUserAccount($oUserSession->getAccount());
        if (null == $userBean) {
            throw new RestException(UserSessionErrorConstant::USER_LOGIN_PARAM_ERROR,
                UserSessionErrorConstant::USER_LOGIN_PARAM_ERROR_CODE,
                RestHttpStatus::Not_Found);
        }

        if ("N" == $userBean->getVerify()) {
            throw new BaException(UserSessionErrorConstant::USER_LOGIN_ERROR_NOT_ALLOWED);
        }
        if ("R" == $userBean->getVerify()) {
            throw new BaException(UserSessionErrorConstant::USER_LOGIN_ERROR_NOT_PASS);
        }
        if ("S" == $userBean->getVerify()) {
            throw new BaException(UserSessionErrorConstant::USER_LOGIN_ERROR_ACCOUNT_STOPPED);
        }

        // 校验密码
        if (strtolower(EncryptUtils::encodePassword($oUserSession->getPwd())) != strtolower($userBean->getPassword())) {
            throw new RestException(UserSessionErrorConstant::USER_LOGIN_PARAM_ERROR,
                UserSessionErrorConstant::USER_LOGIN_PARAM_ERROR_CODE, RestHttpStatus::Unauthorized);
        }

        $_SESSION[$this->_sessionKey] = serialize($userBean);

        return $userBean;
    }

    /**
     * 创建Session.
     * @param UserListBean $userListBean
     */
    public function makeSession($userListBean) {
        $_SESSION[$this->_sessionKey] = serialize($userListBean);
    }


    /**
     * 退出登录.
     */
    public function delete() {
        if (isset($_SESSION[$this->_sessionKey])) {
            unset($_SESSION[$this->_sessionKey]);
        }
    }

    /**
     * 刷新session.
     */
    public function refresh() {
        if (!$this->isLogin()) {
            return;
        }
        $userBean = $this->_userRepository->findOneByUserAccount($this->getSession()->getAccount());
        $_SESSION[$this->_sessionKey] = serialize($userBean);
    }


}