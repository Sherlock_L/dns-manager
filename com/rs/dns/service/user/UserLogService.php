<?php
namespace com\rs\dns\service\user;

/**
 * Interface UserLogService
 * @package php\service\user
 */
interface UserLogService {
    /**
     * 添加日志.
     * @param $nLogType
     * @param $strRemark
     * @return mixed
     */
    public function log($nLogType, $strRemark);
}