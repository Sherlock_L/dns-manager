<?php
namespace com\rs\dns\service\user;

use com\rs\dns\repository\bean\UserListBean;

/**
 * Interface UserService
 * @package php\service\user
 */
interface UserService {
    /**
     * modify user info.
     * @param $userId
     * @param $arrUpdate
     * @return mixed
     */
    public function modify($userId, $arrUpdate);

    /**
     * get user info.
     * @param $userId int userId.
     * @return UserListBean.
     */
    public function getInfo($userId);

    /**
     * 通过用户命获取用户信息.
     * @param string $username 用户名.
     * @return mixed
     */
    public function getInfoByUsername($username);

    /**
     * modify password.
     * @param $userId
     * @param $oldPassword
     * @param $newPassword
     * @return mixed
     */
    public function modifyPassword($userId, $oldPassword, $newPassword);

    /**
     * 接口创建用户.
     * @param string $username
     * @param string $pwd
     * @return mixed
     */
    public function apiCreate($username, $pwd);
}