<?php
/**
 * Created by zj.
 * User: zj
 * Date: 2019/11/13 0013
 * Time: 上午 10:31
 */
namespace com\rs\dns\service\user;


use com\rs\dns\controller\api\vo\UserLoginForm;
use com\rs\dns\repository\bean\UserListBean;

/**
 * Interface UserSessionService
 * @package php\service\user
 */
interface UserSessionService {
    /**
     * 是否已登录.
     * @return bool
     */
    public function isLogin();

    /**
     * 是否为管理员.
     * @return bool
     */
    public function isAdmin();

    /**
     * 是否为超管.
     * @return bool.
     */
    public function isSuper();

    /**
     * 获取用户session.
     * @return UserListBean|null
     */
    public function getSession();

    /**
     * @param $oUserSession UserLoginForm 用户登录表单.
     * @return mixed
     */
    public function login($oUserSession);

    /**
     * 创建session.
     * @param UserListBean $userListBean
     */
    public function makeSession($userListBean);

    /**
     * 退出登录.
     */
    public function delete();

    /**
     * 刷新session.
     */
    public function refresh();
}