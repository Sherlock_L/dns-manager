<?php
namespace com\rs\dns\utils;
use com\rs\dns\service\user\impl\UserLogServiceImpl;
use com\rs\dns\service\user\UserLogService;

/**
 * Class FactoryUtils
 * @package php\utils
 */
class FactoryUtils {
    /**
     * Context Factory class.
     * @var array
     */
    private static $_factory = array();

    /**
     * @return UserLogService
     */
    public static function logService() {
        if (!isset(self::$_factory['logService'])) {
            self::$_factory['logService'] = new UserLogServiceImpl();
        }
        return self::$_factory['logService'];
    }
}