<?php
namespace com\rs\dns\controller\view;

use com\rs\dns\controller\BaseController;

/**
 * Class ManageController
 * @package com\controller\view
 * @RequestMapping("")
 */
class ManageController extends BaseController {
    /**
     * 未定义页面访问.
     * @RequestMapping(value="/undefined", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function undefined() {
        $this->_tpl->display('bs-undefined.tpl');
    }

    /**
     * 管理中心首页
     * @RequestMapping(value="/manage-index", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function manageIndex() {
        $this->_tpl->display('bk-manage-index.tpl');
    }

    /**
     * 管理中心-会员管理.
     * @RequestMapping(value="/manage-member", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function manageMember () {
        $this->_tpl->display('bk-manage-member.tpl');
    }

    /**
     * 管理中心-域名列表.
     * @RequestMapping(value="/manage-domains", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function manageDomains() {
        $this->_tpl->display('bk-manage-domains.tpl');
    }

    /**
     * 管理中心-解析记录.
     * @RequestMapping(value="/manage-records", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function manageRecords() {
        $this->_tpl->display('bk-manage-records.tpl');
    }

    /**
     * 管理中心-解析-DNS
     * @RequestMapping(value="/manage-records-dns", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function manageRecordsDns() {
        $this->_tpl->display('bk-manage-records-dns.tpl');
    }

    /**
     * 管理中心-解析-负载均衡
     * @RequestMapping(value="/manage-records-balance", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function manageRecordsBalance() {
        $this->_tpl->display('bk-manage-records-balance.tpl');
    }

    /**
     * 管理中心-解析-转发
     * @RequestMapping(value="/manage-records-url", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function manageRecordsUrl() {
        $this->_tpl->display("bk-manage-records-url.tpl");
    }

    /**
     * 管理中心-线路管理.
     * @RequestMapping(value="/manage-network", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function manageNetwork() {
        $this->_tpl->display('bk-manage-network.tpl');
    }

    /**
     * 管理中心-IPV4分配表.
     * @RequestMapping(value="/manage-ipv4", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function manageIpv4() {
        $this->_tpl->display('bk-manage-ipv4.tpl');
    }

    /**
     * 管理中心-IPV6分配表.
     * @RequestMapping(value="/manage-ipv6", method="GET")
     */
    public function manageIpv6() {
        $this->_tpl->display('bk-manage-ipv6.tpl');
    }

    /**
     * 管理中心-宕机检测.
     * @RequestMapping(value="/manage-hosts", method="GET")
     */
    public function manageHosts() {
        $this->_tpl->display('bk-manage-hosts.tpl');
    }

    /**
     * 管理中心-接口配置.
     * @RequestMapping(value="/manage-config", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function manageConfig() {
        $this->_tpl->display('bk-manage-config.tpl');
    }
}