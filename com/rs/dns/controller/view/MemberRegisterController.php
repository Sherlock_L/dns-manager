<?php
namespace com\rs\dns\controller\view;

use com\rs\dns\controller\BaseController;
use com\rs\dns\service\MemberRandCodeService;
use restphp\validate\image\RestValidateImage;

/**
 * Class MemberRegisterController
 * @RequestMapping("")
 * @package com\controller\view
 */
class MemberRegisterController extends BaseController {
    /**
     * 会员中心.
     * @RequestMapping(value=["/", "/index.html", "/members.html"], method="GET")
     */
    public function members() {
        if ($this->_userSession->isLogin()) {
            if ($this->_userSession->isAdmin() || $this->_userSession->isSuper()) {
                $this->_tpl->display('bk-manage.tpl');
            } else {
                $this->_tpl->display('bm-member.tpl');
            }
        } else {
            $this->_tpl->display('bs-login.tpl');
        }
    }

    /**
     * 随机验证码.
     * @RequestMapping(value="/members/randcode", method="GET")
     */
    public function randCode() {
        RestValidateImage::responseImage(MemberRandCodeService::createCode());
    }

    /**
     * 会员注册.
     * @RequestMapping(value="/member-register.html", method="GET")
     */
    public function memberRegister() {
        $this->_tpl->display('bs-member-register.tpl');
    }
}