<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class IPQuery
 * @package com\rs\dns\controller\api\vo
 */
final class IPQuery {
    /**
     * @var string 网络ID.
     */
    private $_netId;
    /**
     * @var string 关键字.
     */
    private $_keyword;

    /**
     * @return string
     */
    public function getNetId()
    {
        return $this->_netId;
    }

    /**
     * @param string $netId
     */
    public function setNetId($netId)
    {
        $this->_netId = $netId;
    }

    /**
     * @return string
     */
    public function getKeyword()
    {
        return $this->_keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword($keyword)
    {
        $this->_keyword = $keyword;
    }


}