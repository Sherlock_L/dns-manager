<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class RecordForm
 * @package com\rs\dns\controller\api\vo
 */
final class RecordForm {
    /**
     * @var string 域名.
     * @xxxcustomer(method="\com\rs\dns\controller\api\validate\ValidateDomain::exist",message="域名不存在")
     */
    private $_origin;

    /**
     * @var string 主机名.
     */
    private $_name;

    /**
     * @var string 类型.
     * @inArray(value=[A|AAAA|MX|CNAME|NS|PTR|TXT|SRV],message="解析类型值不正确")
     */
    private $_type;

    /**
     * @var int 优先级
     * @range(min=1)
     */
    private $_aux;

    /**
     * @var int ttl
     * @range(min=1)
     */
    private $_ttl;

    /**
     * @var string 网络组ID.
     * @range(min=0)
     */
    private $_netid;

    /**
     * @var string 值.
     * @notnull(message="记录值不能为空")
     * @customer(method="\com\rs\dns\controller\api\validate\ValidateRecordData::check")
     */
    private $_data;

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->_origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin($origin)
    {
        $this->_origin = $origin;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->_type = $type;
    }

    /**
     * @return int
     */
    public function getAux()
    {
        return $this->_aux;
    }

    /**
     * @param int $aux
     */
    public function setAux($aux)
    {
        $this->_aux = $aux;
    }

    /**
     * @return int
     */
    public function getTtl()
    {
        return $this->_ttl;
    }

    /**
     * @param int $ttl
     */
    public function setTtl($ttl)
    {
        $this->_ttl = $ttl;
    }

    /**
     * @return string
     */
    public function getNetid()
    {
        return $this->_netid;
    }

    /**
     * @param string $netid
     */
    public function setNetid($netid)
    {
        $this->_netid = $netid;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->_data = $data;
    }
}