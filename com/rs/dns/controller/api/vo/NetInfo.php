<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class NetInfo
 * @package com\rs\dns\controller\api\vo
 */
final class NetInfo {
    /**
     * @var integer 线路ID.
     */
    private $_id;

    /**
     * @var string 线路名称.
     */
    private $_netname;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getNetname()
    {
        return $this->_netname;
    }

    /**
     * @param string $netname
     */
    public function setNetname($netname)
    {
        $this->_netname = $netname;
    }


}