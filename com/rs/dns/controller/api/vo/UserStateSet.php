<?php
namespace com\rs\dns\controller\api\vo;

final class UserStateSet {
    /**
     * @var string 状态.
     * @inArray(value=['N'|'Y'|'ZS'|'ZR'],message=状态取值范围不正确)
     */
    private $_verify;

    /**
     * @var array 需要操作的用户ID.
     * @notempty(message="需要操作的用户不为空")
     */
    private $_userIdList;

    /**
     * @return string
     */
    public function getVerify()
    {
        return $this->_verify;
    }

    /**
     * @param string $verify
     */
    public function setVerify($verify)
    {
        $this->_verify = $verify;
    }

    /**
     * @return array
     */
    public function getUserIdList()
    {
        return $this->_userIdList;
    }

    /**
     * @param array $userIdList
     */
    public function setUserIdList($userIdList)
    {
        $this->_userIdList = $userIdList;
    }
}
