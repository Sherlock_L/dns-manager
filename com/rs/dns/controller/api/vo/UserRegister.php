<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class UserRegister
 * @package com\controller\api\vo
 */
final class UserRegister {
    /**
     * @var string 会员账号.
     * @length(min=4,max=20,message=会员账号长度为4-20个字符)
     */
    private $_account;

    /**
     * @var string 登录密码.
     * @length(min=6,max=16,message=会员账号长度为6-16个字符)
     */
    private $_passwd;

    /**
     * @var string 真实姓名.
     * @length(min=1,max=20,message=真实姓名长度为1-20个字符)
     */
    private $_realName;

    /**
     * @var string 手机号码.
     * @mobile(message=请输入正确的手机号码)
     */
    private $_mobile;

    /**
     * @var string 验证码.
     * @length(min=4,max=4,message=请输入正确的验证码)
     */
    private $_randCode;

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->_account;
    }

    /**
     * @param string $account
     */
    public function setAccount($account)
    {
        $this->_account = $account;
    }

    /**
     * @return string
     */
    public function getPasswd()
    {
        return $this->_passwd;
    }

    /**
     * @param string $passwd
     */
    public function setPasswd($passwd)
    {
        $this->_passwd = $passwd;
    }

    /**
     * @return string
     */
    public function getRealName()
    {
        return $this->_realName;
    }

    /**
     * @param string $realName
     */
    public function setRealName($realName)
    {
        $this->_realName = $realName;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->_mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->_mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getRandCode()
    {
        return $this->_randCode;
    }

    /**
     * @param string $randCode
     */
    public function setRandCode($randCode)
    {
        $this->_randCode = $randCode;
    }


}