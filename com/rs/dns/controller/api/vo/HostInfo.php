<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class HostInfo
 * @package com\rs\dns\controller\api\vo
 */
final class HostInfo {
    /**
     * @var integer 行号.
     */
    private $_RowNumber;

    /**
     * @var string ip.
     */
    private $_IP;

    /**
     * @var integer 端口.
     */
    private $_port;

    /**
     * @var string 状态.
     */
    private $_status;

    /**
     * @return int
     */
    public function getRowNumber()
    {
        return $this->_RowNumber;
    }

    /**
     * @param int $RowNumber
     */
    public function setRowNumber($RowNumber)
    {
        $this->_RowNumber = $RowNumber;
    }

    /**
     * @return string
     */
    public function getIP()
    {
        return $this->_IP;
    }

    /**
     * @param string $IP
     */
    public function setIP($IP)
    {
        $this->_IP = $IP;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->_port;
    }

    /**
     * @param int $port
     */
    public function setPort($port)
    {
        $this->_port = $port;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->_status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->_status = $status;
    }
}