<?php
namespace com\rs\dns\controller\api\vo;

final class DomainStateSet {
    /**
     * @var string 状态.
     * @inArray(value=['0'|'1'],message=状态取值范围不正确)
     */
    private $_yn;

    /**
     * @var array 需要操作的用户ID.
     * @notempty(message="需要操作的用户不为空")
     */
    private $_domainIdList;

    /**
     * @return string
     */
    public function getYn()
    {
        return $this->_yn;
    }

    /**
     * @param string $verify
     */
    public function setYn($verify)
    {
        $this->_yn = $verify;
    }

    /**
     * @return array
     */
    public function getDomainIdList()
    {
        return $this->_domainIdList;
    }

    /**
     * @param array $userIdList
     */
    public function setDomainIdList($userIdList)
    {
        $this->_domainIdList = $userIdList;
    }
}
