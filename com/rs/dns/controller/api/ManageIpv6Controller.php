<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\CommonConstant;
use com\rs\dns\controller\api\vo\IPQuery;
use com\rs\dns\controller\api\vo\Ipv6BatchForm;
use com\rs\dns\controller\api\vo\Ipv6Form;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\Ipv6Service;
use restphp\http\RestHttpRequest;

/**
 * Class ManageIpv6Controller
 * @package com\rs\dns\controller
 * @RequestMapping("/api/manage/ipv6s")
 */
final class ManageIpv6Controller extends BaseController {
    /**
     * 获取IPV6列表.
     * @RequestMapping(value="", method="GET")
     */
    public function getIpList() {
        $pageParam = RestHttpRequest::getPageParam();
        $ipQuery = RestHttpRequest::getParameterAsObject(new IPQuery());
        $ipList = Ipv6Service::getIpList($ipQuery, $pageParam);
        $this->_success($ipList);
    }

    /**
     * 添加IPV6地址分配.
     * @RequestMapping(value="", method="POST")
     * @throws \ReflectionException
     */
    public function add() {
        $ipv6Form = RestHttpRequest::getBody(new Ipv6Form(), true);
        Ipv6Service::add($ipv6Form);
    }

    /**
     * 修改IPV6地址分配.
     * @RequestMapping(value="/{id}", method="PUT")
     * @throws \ReflectionException
     */
    public function modify() {
        $id = RestHttpRequest::getPathValue("id");
        $ipv6Form = RestHttpRequest::getBody(new Ipv6Form(), true);
        Ipv6Service::modify($id, $ipv6Form);
    }

    /**
     * 删除IPV6地址.
     * @RequestMapping(value="/{id}", method="DELETE")
     */
    public function delete() {
        $id = RestHttpRequest::getPathValue("id");
        Ipv6Service::delete($id);
    }

    /**
     * 批量删除.
     * @RequestMapping(value="/actions/delete", method="POST")
     * @throws BaException
     */
    public function deleteBatch() {
        $arrId = RestHttpRequest::getBody();
        if (null == $arrId || empty($arrId)) {
            throw new BaException(CommonConstant::DATA_TO_DELETE_CAN_NOT_NULL);
        }
        Ipv6Service::deleteBatch($arrId);
    }

    /**
     * 批量新增.
     * @RequestMapping(value="/actions/batch_add", method="POST")
     * @throws \ReflectionException
     */
    public function batchAdd() {
        $ipv6BatchForm = RestHttpRequest::getBody(new Ipv6BatchForm(), true);
        Ipv6Service::batchAdd($ipv6BatchForm);
    }
}