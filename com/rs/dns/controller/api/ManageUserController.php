<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\UserManageConst;
use com\rs\dns\controller\api\vo\UserManage;
use com\rs\dns\controller\api\vo\UserQuery;
use com\rs\dns\controller\api\vo\UserStateSet;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\user\impl\UserManageServiceImpl;
use restphp\http\RestHttpRequest;
use restphp\utils\RestClassUtils;
use restphp\validate\RestValidate;

/**
 * Class ManageUserController
 * @package com\controller\api
 * @RequestMapping("/api/manage/users")
 */
class ManageUserController extends BaseController {
    /**
     * 获取用户列表.
     * @RequestMapping(value="", method="GET")
     */
    public function getUserList() {
        $params = RestHttpRequest::getParameterAsObject(new UserQuery());
        $pageParam = RestHttpRequest::getPageParam();

        $userManageService = new UserManageServiceImpl();
        $result = $userManageService->getUserList($params, $pageParam);
        $this->_success($result);
    }

    /**
     * 添加用户.
     * @RequestMapping(value="", method="POST")
     * @throws \ReflectionException
     */
    public function addUser() {
        $user = RestHttpRequest::getRequestBody(new UserManage());
        RestValidate::execute($user);

        $userManageService = new UserManageServiceImpl();

        $arrUser = RestClassUtils::beanToArr($user);

        $userManageService->addUser($arrUser);
    }

    /**
     * 修改用户信息.
     * @RequestMapping(value="/{userId}, method="PUT")
     * @throws \ReflectionException
     * @throws \restphp\exception\RestException
     */
    public function modifyUser() {
        $user = RestHttpRequest::getRequestBody(new UserManage());
        RestValidate::execute($user);

        $userId = RestHttpRequest::getPathValue("userId");
        $userManageService = new UserManageServiceImpl();

        $arrUser = RestClassUtils::beanToArr($user);

        $userManageService->modifyUser($userId, $arrUser);
    }

    /**
     * 删除用户.
     * @RequestMapping(value="/{userId}, method="DELETE")
     * @throws \restphp\exception\RestException
     */
    public function deleteUser() {
        $userId = RestHttpRequest::getPathValue("userId");
        if ($userId == $this->_getUserId()) {
            throw new BaException(UserManageConst::CAN_NOT_DELETE_SELF);
        }
        $userManageService = new UserManageServiceImpl();
        $userManageService->deleteUser($userId);
    }

    /**
     * 批量删除用户.
     * @RequestMapping(value="/actions/delete", method="POST")
     */
    public function deleteUserBatch() {
        $arrId = RestHttpRequest::getRequestBody();
        $arrId = array_diff($arrId, [$this->_getUserId()]);
        if (empty($arrId)) {
            throw new BaException(UserManageConst::CAN_NOT_DELETE_SELF);
        }
        $userManageService = new UserManageServiceImpl();
        $userManageService->deleteUserBatch($arrId);
    }

    /**
     * 批量修改用户状态.
     * @RequestMapping(value="/actions/state", method="POST")
     */
    public function batchState() {
        $userStateSet = RestHttpRequest::getRequestBody(new UserStateSet(), true);
        $arrUserId = array_diff($userStateSet->getUserIdList(), [$this->_getUserId()]);
        if (empty($arrUserId)) {
            throw new BaException(UserManageConst::CAN_NOT_DELETE_SELF);
        }
        $userStateSet->setUserIdList($arrUserId);

        $userManageService = new UserManageServiceImpl();
        $userManageService->batchSetState($userStateSet);
    }
}