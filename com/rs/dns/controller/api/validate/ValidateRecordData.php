<?php
namespace com\rs\dns\controller\api\validate;

use com\rs\dns\constant\RecordConstant;
use com\rs\dns\controller\api\vo\RecordForm;
use com\rs\dns\exception\BaException;
use restphp\exception\RestException;
use restphp\utils\RestStringUtils;

/**
 * Class ValidateRecordData
 * @package com\rs\dns\controller\api\validate
 */
final class ValidateRecordData {
    /**
     * 参数是否正确.
     * @param $value
     * @param $message
     * @param $propName
     * @param RecordForm $classInstance
     * @throws BaException
     * @throws RestException
     */
    public static function check($value, $message, $propName, $classInstance) {
        $value = trim($value);
        if (RestStringUtils::isBlank($value)) {
            throw new BaException(RecordConstant::RECORD_DATA_NOT_NULL);
        }

        if ('A' == $classInstance->getType() && !RestStringUtils::isIpv4($value)) {
            throw new BaException(RecordConstant::RECORD_DATA_MUST_IPV4);
        } else if ('AAAA' == $classInstance->getType() && !RestStringUtils::isIpv6($value)) {
            throw new BaException(RecordConstant::RECORD_DATA_MUST_IPV6);
        } else if (in_array($classInstance->getType(), array('MX','CNAME')) && !RestStringUtils::isDomain($value)) {
            throw new BaException(RecordConstant::RECORD_DATA_MUST_DOMAIN);
        } else if ('SRV' == $classInstance->getType() && !RestStringUtils::isSRV($value)) {
            throw new BaException(RecordConstant::RECORD_DATA_MUST_SRV);
        }
    }
}