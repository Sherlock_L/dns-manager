<?php
namespace com\rs\dns\controller\api\validate;

use com\rs\dns\constant\NetConstant;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\NetService;
use restphp\exception\RestException;
use restphp\http\RestHttpStatus;
use restphp\utils\RestStringUtils;

/**
 * Class ValidateNetId
 * @package com\rs\dns\controller\api\validate
 */
final class ValidateNetId {
    /**
     * 是否存在.
     * @param $value
     * @param $message
     * @param $propName
     * @param $classInstance
     * @throws BaException
     * @throws RestException
     */
    public static function exist($value, $message, $propName, $classInstance) {
        if (RestStringUtils::isBlank($value)) {
            if (RestStringUtils::isBlank($message)) {
                throw new BaException(NetConstant::NET_NOT_EXISTS);
            }
            throw new RestException($message, APP_NAME.'/'.NetConstant::NET_NOT_EXISTS, RestHttpStatus::Bad_Request, array($propName));
        }
        $netInfo = NetService::getOne($value);
        if (null == $netInfo) {
            if (RestStringUtils::isBlank($message)) {
                throw new BaException(NetConstant::NET_NOT_EXISTS);
            }
            throw new RestException($message, APP_NAME.'/'.NetConstant::NET_NOT_EXISTS, RestHttpStatus::Bad_Request, array($propName));
        }
    }
}