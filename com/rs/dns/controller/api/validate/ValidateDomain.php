<?php
namespace com\rs\dns\controller\api\validate;

use com\rs\dns\constant\DomainConstant;
use com\rs\dns\controller\api\vo\RecordForm;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\DomainService;
use restphp\exception\RestException;
use restphp\http\RestHttpStatus;
use restphp\utils\RestStringUtils;
use restphp\validate\RestValidate;

/**
 * Class ValidateDomain
 * @package com\rs\dns\controller\api\validate
 */
final class ValidateDomain {
    /**
     * 域名是否存在.
     * @param $value
     * @param $message
     * @param $propName
     * @param RecordForm $classInstance
     * @throws BaException
     * @throws RestException
     */
    public static function exist($value, $message, $propName, $classInstance) {
        $value = trim($value);
        $message = RestValidate::clearMessageBoundary($message);
        if (RestStringUtils::isBlank($value)) {
            if (RestStringUtils::isBlank($message)) {
                throw new BaException(DomainConstant::DOMAIN_NOT_EXISTS);
            }
            throw new RestException($message, APP_NAME.'/'.DomainConstant::DOMAIN_NOT_EXISTS, RestHttpStatus::Bad_Request, array($propName));
        }

        $exist = DomainService::getInfoByDomain($value.".");
        if (null == $exist) {
            if (RestStringUtils::isBlank($message)) {
                throw new BaException(DomainConstant::DOMAIN_NOT_EXISTS);
            }
            throw new RestException($message, APP_NAME.'/'.DomainConstant::DOMAIN_NOT_EXISTS, RestHttpStatus::Bad_Request, array($propName));
        }
    }
}