<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\CommonConstant;
use com\rs\dns\controller\api\vo\IPQuery;
use com\rs\dns\controller\api\vo\Ipv4BatchForm;
use com\rs\dns\controller\api\vo\Ipv4Form;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\Ipv4Service;
use restphp\http\RestHttpRequest;

/**
 * Class ManageIpv4Controller
 * @package com\rs\dns\controller
 * @RequestMapping("/api/manage/ipv4s")
 */
final class ManageIpv4Controller extends BaseController {
    /**
     * 获取IPV4列表.
     * @RequestMapping(value="", method="GET")
     */
    public function getIpList() {
        $pageParam = RestHttpRequest::getPageParam();
        $ipQuery = RestHttpRequest::getParameterAsObject(new IPQuery());
        $ipList = Ipv4Service::getIpList($ipQuery, $pageParam);
        $this->_success($ipList);
    }

    /**
     * 添加IPV4地址分配.
     * @RequestMapping(value="", method="POST")
     * @throws \ReflectionException
     */
    public function add() {
        $ipv4Form = RestHttpRequest::getBody(new Ipv4Form(), true);
        Ipv4Service::add($ipv4Form);
    }

    /**
     * 修改IPV4地址分配.
     * @RequestMapping(value="/{id}", method="PUT")
     * @throws \ReflectionException
     */
    public function modify() {
        $id = RestHttpRequest::getPathValue("id");
        $ipv4Form = RestHttpRequest::getBody(new Ipv4Form(), true);
        Ipv4Service::modify($id, $ipv4Form);
    }

    /**
     * 删除IPV4地址.
     * @RequestMapping(value="/{id}", method="DELETE")
     */
    public function delete() {
        $id = RestHttpRequest::getPathValue("id");
        Ipv4Service::delete($id);
    }

    /**
     * 批量删除.
     * @RequestMapping(value="/actions/delete", method="POST")
     * @throws BaException
     */
    public function deleteBatch() {
        $arrId = RestHttpRequest::getBody();
        if (null == $arrId || empty($arrId)) {
            throw new BaException(CommonConstant::DATA_TO_DELETE_CAN_NOT_NULL);
        }
        Ipv4Service::deleteBatch($arrId);
    }

    /**
     * 批量新增.
     * @RequestMapping(value="/actions/batch_add", method="POST")
     * @throws \ReflectionException
     */
    public function batchAdd() {
        $ipv4BatchForm = RestHttpRequest::getBody(new Ipv4BatchForm(), true);
        Ipv4Service::batchAdd($ipv4BatchForm);
    }
}