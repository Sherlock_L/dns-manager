<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\DomainConstant;
use com\rs\dns\controller\api\vo\DomainForm;
use com\rs\dns\controller\api\vo\DomainQuery;
use com\rs\dns\controller\api\vo\DomainStateSet;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\DomainService;
use restphp\http\RestHttpRequest;


/**
 * Class ManageDomainController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/manage/domains")
 */
final class ManageDomainController extends BaseController {
    /**
     * 域名管理列表.
     * @RequestMapping(value="", method="GET")
     */
    public function domainList() {
        $pageParam = RestHttpRequest::getPageParam();
        $domainQuery = RestHttpRequest::getParameterAsObject(new DomainQuery());
        $domainList = DomainService::getDomainList($domainQuery, $pageParam);
        $this->_success($domainList);
    }

    /**
     * 新增域名.
     * @RequestMapping(value="", method="POST")
     * @throws BaException
     */
    public function addDomain() {
        $domainForm = RestHttpRequest::getRequestBody(new DomainForm(), true);
        DomainService::add($domainForm);
    }

    /**
     * 修改域名信息.
     * @RequestMapping(value="/{id}", method="PUT")
     * @throws BaException
     */
    public function modifyDomain() {
        $domainForm = RestHttpRequest::getRequestBody(new DomainForm(), true);
        $id = RestHttpRequest::getPathValue("id");
        DomainService::modify($id, $domainForm);
    }

    /**
     * 单个删除域名.
     * @RequestMapping(value="/{id}", method="DELETE")
     */
    public function deleteDomain() {
        $id = RestHttpRequest::getPathValue("id");
        DomainService::delete($id);
    }

    /**
     * 设置域名状态.
     * @RequestMapping(value="/actions/state", method="POST")
     * @throws BaException
     */
    public function setDomainState() {
        $domainStateSet = RestHttpRequest::getRequestBody(new DomainStateSet(), true);
        if (empty($domainStateSet->getDomainIdList())) {
            throw new BaException(DomainConstant::DOMAIN_TO_DELETE_CAN_NOT_NULL);
        }
        DomainService::setState($domainStateSet);
    }

    /**
     * 批量删除域名.
     * @RequestMapping(value="/actions/delete", method="POST")
     * @throws BaException
     */
    public function deleteDomainBatch() {
        $arrId = RestHttpRequest::getRequestBody();
        if (null == $arrId || empty($arrId)) {
            throw new BaException(DomainConstant::DOMAIN_TO_DELETE_CAN_NOT_NULL);
        }
        DomainService::batchDelete($arrId);
    }
}