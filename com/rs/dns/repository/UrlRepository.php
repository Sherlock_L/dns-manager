<?php
namespace com\rs\dns\repository;

use restphp\driver\RestMSRepository;

/**
 * Class UrlRepository
 * @package com\rs\dns\repository
 */
final class UrlRepository extends RestMSRepository {
    public function __construct() {
        parent::__construct('url');
    }
}