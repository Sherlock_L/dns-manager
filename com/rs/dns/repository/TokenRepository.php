<?php
namespace com\rs\dns\repository;

use restphp\driver\RestMSRepository;

/**
 * Class TokenRepository
 * @package com\rs\dns\repository
 */
final class TokenRepository extends RestMSRepository {
    public function __construct(){
        parent::__construct('token');
    }
}