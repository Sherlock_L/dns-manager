<?php
namespace com\rs\dns\repository;

use restphp\driver\RestMSRepository;

/**
 * Class HostCheckRepository
 * @package com\rs\dns\repository
 */
final class HostCheckRepository extends RestMSRepository {
    public function __construct() {
        parent::__construct('hostcheck');
    }
}