<?php
namespace com\rs\dns\repository\bean;

/**
 * Class TokenBean.
 * @package com\rs\dns\repository\bean
 */
final class TokenBean {
    /**
     * @var int 主键ID.
     */
    private $_id;

    /**
     * @var string token.
     */
    private $_token;

    /**
     * @var string 过期时间
     */
    private $_expireAt;

    /**
     * @var int 用户ID.
     */
    private $_userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->_token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->_token = $token;
    }

    /**
     * @return string
     */
    public function getExpireAt()
    {
        return $this->_expireAt;
    }

    /**
     * @param string $expireAt
     */
    public function setExpireAt($expireAt)
    {
        $this->_expireAt = $expireAt;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->_userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->_userId = $userId;
    }
}