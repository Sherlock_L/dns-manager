<?php
namespace com\rs\dns\repository\bean;

/**
 * Class UserListBean
 * @package php\repository\bean
 */
class UserListBean {
    private $_ID;
    private $_username;
    private $_password;
    private $_realname;
    private $_tel;
    private $_address;
    private $_postcode;
    private $_email;
    private $_msn;
    private $_icq;
    private $_isadmin;
    private $_soanum;
    private $_rrnum;
    private $_verify;
    private $_regtime;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->_ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID)
    {
        $this->_ID = $ID;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }

    /**
     * @return mixed
     */
    public function getRealname()
    {
        return $this->_realname;
    }

    /**
     * @param mixed $realname
     */
    public function setRealname($realname)
    {
        $this->_realname = $realname;
    }

    /**
     * @return mixed
     */
    public function getTel()
    {
        return $this->_tel;
    }

    /**
     * @param mixed $tel
     */
    public function setTel($tel)
    {
        $this->_tel = $tel;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->_address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->_address = $address;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->_postcode;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcode($postcode)
    {
        $this->_postcode = $postcode;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }

    /**
     * @return mixed
     */
    public function getMsn()
    {
        return $this->_msn;
    }

    /**
     * @param mixed $msn
     */
    public function setMsn($msn)
    {
        $this->_msn = $msn;
    }

    /**
     * @return mixed
     */
    public function getIcq()
    {
        return $this->_icq;
    }

    /**
     * @param mixed $icq
     */
    public function setIcq($icq)
    {
        $this->_icq = $icq;
    }

    /**
     * @return mixed
     */
    public function getIsadmin()
    {
        return $this->_isadmin;
    }

    /**
     * @param mixed $isadmin
     */
    public function setIsadmin($isadmin)
    {
        $this->_isadmin = $isadmin;
    }

    /**
     * @return mixed
     */
    public function getSoanum()
    {
        return $this->_soanum;
    }

    /**
     * @param mixed $soanum
     */
    public function setSoanum($soanum)
    {
        $this->_soanum = $soanum;
    }

    /**
     * @return mixed
     */
    public function getRrnum()
    {
        return $this->_rrnum;
    }

    /**
     * @param mixed $rrnum
     */
    public function setRrnum($rrnum)
    {
        $this->_rrnum = $rrnum;
    }

    /**
     * @return mixed
     */
    public function getVerify()
    {
        return $this->_verify;
    }

    /**
     * @param mixed $verify
     */
    public function setVerify($verify)
    {
        $this->_verify = $verify;
    }

    /**
     * @return mixed
     */
    public function getRegtime()
    {
        return $this->_regtime;
    }

    /**
     * @param mixed $regtime
     */
    public function setRegtime($regtime)
    {
        $this->_regtime = $regtime;
    }
}